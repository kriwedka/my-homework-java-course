package Java13_PM_DZ_4;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/*
На вход подается список вещественных чисел. Необходимо отсортировать их по
убыванию.
 */
public class Task_4 {
    public static void main(String[] args) {
        List<Integer> arg = Arrays.asList(2, 6, 3, 8, 10);
        List<Integer> sorted = arg.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
        System.out.println(sorted);
    }
}
