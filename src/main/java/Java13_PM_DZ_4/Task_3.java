package Java13_PM_DZ_4;

import java.util.Arrays;
import java.util.List;

/*
3. На вход подается список строк. Необходимо вывести количество непустых строк в
списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */
public class Task_3 {
    public static void main(String[] args) {
        List<String> strings = Arrays.asList("abc", "", "", "def", "qqq");
        int count = (int) strings.parallelStream().filter(string -> !string.isEmpty()).count();
        System.out.println(count);
    }
}
