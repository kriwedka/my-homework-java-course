package Java13_PM_DZ_4;

import java.util.List;
import java.util.stream.Collectors;

/*
На вход подается список непустых строк. Необходимо привести все символы строк к
верхнему регистру и вывести их, разделяя запятой.
Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.
 */
public class Task_5 {
    public static void main(String[] args) {
        List<String> strings = List.of("abc", "def", "qqq");
        List<String> result = strings.stream()
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.println(result);
    }
}
