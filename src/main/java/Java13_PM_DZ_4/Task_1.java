package Java13_PM_DZ_4;

import java.util.stream.IntStream;
/*
Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран.
 */
public class Task_1 {
    public static void main(String[] args) {
        int start = 1;
        int end = 100;
        int sum = IntStream.range(start, 1 + end).filter(num -> 0 == num % 2).sum();
        System.out.println(sum);
    }
}
