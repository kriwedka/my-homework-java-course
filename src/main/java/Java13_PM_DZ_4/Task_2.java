package Java13_PM_DZ_4;
/*
На вход подается список целых чисел. Необходимо вывести результат перемножения
этих чисел.
Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
 */
import java.util.ArrayList;
import java.util.List;

public class Task_2 {
    public static void main(String[] args) {
        List<Integer> arg = new ArrayList<>();
        arg.add(1);
        arg.add(2);
        arg.add(3);
        arg.add(4);
        arg.add(5);
        Integer multi = arg.stream()
                .reduce(1,(a, b) -> a * b);
        System.out.println(multi);
    }
}
