package Java12_BM_W1_DZ_1_Part_1;

//Прочитайте из консоли имя пользователя и выведите в консоль строку:
//Привет, <имя пользователя>!
//Подсказка:
//Получите данные из консоли c помощью объекта Scanner, сохраните в
//переменную userName и выведите в консоль с помощью System.out.println()

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String userName = scan.next();

        System.out.println("Hello, " + userName + "!");
    }
}
