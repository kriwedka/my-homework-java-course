package Java12_BM_W1_DZ_1_Part_1;

//На вход подается количество километров count. Переведите километры в мили
//(1 миля = 1,60934 км) и выведите количество миль.
//Пример:
//Входные данные Выходные данные
//7 4.349609156548647
//143 88.85630134092237

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double kilometers = sc.nextDouble();
        double miles = kilometers / 1.60934;
        System.out.println(miles);
    }
}
