package Java12_BM_W1_DZ_1_Part_1;

//Вычислите и выведите на экран объем шара, получив его радиус r с консоли.
//Подсказка: считать по формуле V = 4/3 * pi * r^3. Значение числа pi взять из
//Math.
//Ограничения:
//0 < r < 100
//Пример:
//Входные данные Выходные данные
//9 3053.6280592892786
//25 65449.84694978735

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int r = input.nextInt();
        double V = (4.0 / 3) * Math.PI * Math.pow(r, 3);
        System.out.println(V);
    }
}
