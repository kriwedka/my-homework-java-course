package Java12_BM_W1_DZ_1_Part_1;

//На вход подается двузначное число n. Выведите число, полученное
//перестановкой цифр в исходном числе n. Если после перестановки получается
//ведущий 0, его также надо вывести.
//Ограничения:
//9 < count < 100
//Пример:
//Входные данные Выходные данные
//45 54
//10 01

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int a = n % 10;
        int b = n / 10;
        System.out.println(a + "" + b);
    }
}
