package Java12_BM_W1_DZ_1_Part_1;

//На вход подается количество секунд, прошедших с начала текущего дня – count.
//Выведите в консоль текущее время в формате: часы и минуты.
//Ограничения:
//0 < count < 86400
//Пример:
//Входные данные Выходные данные
//32433 9 0
//41812 11 36

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        final int SECONDS_PER_MINUTE = 60, MINUTES_PER_HOUR = 60, HOURS_PER_DAY = 24;
        int currentSecond, totalMinutes, currentMinute, totalHours, currentHour;

        Scanner sc = new Scanner(System.in);
        currentSecond = sc.nextInt();

        totalMinutes = currentSecond / SECONDS_PER_MINUTE;
        currentMinute = totalMinutes % MINUTES_PER_HOUR;
        totalHours = totalMinutes / MINUTES_PER_HOUR;
        currentHour = totalHours % HOURS_PER_DAY;

        System.out.print(currentHour + " " + currentMinute);
    }
}
