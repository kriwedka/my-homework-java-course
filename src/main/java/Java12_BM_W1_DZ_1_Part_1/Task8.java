package Java12_BM_W1_DZ_1_Part_1;

import java.util.Scanner;

//На вход подается баланс счета в банке – n. Рассчитайте дневной бюджет на 30
//дней.
//Входные данные Выходные данные
//13509 450.3
//81529 2717.633333333333
public class Task8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        double dailyBudget = n / 30.0;
        System.out.println(dailyBudget);
    }
}
