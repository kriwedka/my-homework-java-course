package Java12_BM_W1_DZ_1_Part_1;

//На вход подается два целых числа a и b. Вычислите и выведите среднее
//квадратическое a и b.
//Подсказка:
//Среднее квадратическое: https://en.wikipedia.org/wiki/Root_mean_square
//Для вычисления квадратного корня воспользуйтесь функцией Math.sqrt(x)
//Пример:
//Входные данные Выходные данные
//35 5 25.0
//23 70 52.100863716449076

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();

        double quadraticMean = Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2);

        System.out.println(quadraticMean);
    }
}
