package Java12_BM_W1_DZ_1_Part_1;

//Переведите дюймы в сантиметры (1 дюйм = 2,54 сантиметров). На вход
//подается количество дюймов, выведите количество сантиметров.
//Пример:
//Входные данные Выходные данные
//12 30.48
//99 251.46

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double inch = sc.nextDouble();
        double centimetre = inch * 2.54;
        System.out.println(centimetre);
    }
}
