package Java12_BM_W1_DZ_1_Part_1;

import java.util.Scanner;

//На вход подается бюджет мероприятия – n тугриков. Бюджет на одного гостя – k
//тугриков. Вычислите и выведите, сколько гостей можно пригласить на
//мероприятие.
//Пример:
//Входные данные Выходные данные
//14185 72 197
//85177 89 957
public class Task9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        int numberOfGuests = n / k;
        System.out.println(numberOfGuests);
    }
}
