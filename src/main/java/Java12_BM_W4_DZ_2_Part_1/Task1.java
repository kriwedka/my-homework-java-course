package Java12_BM_W4_DZ_2_Part_1;
/*
На вход подается число N — длина массива. Затем передается массив
вещественных чисел (ai) из N элементов.
Необходимо реализовать метод, который принимает на вход полученный
массив и возвращает среднее арифметическое всех чисел массива.
Вывести среднее арифметическое на экран.
Ограничения:
● 0 < N < 100
● 0 < ai < 1000
Пример:
Входные данные Выходные данные
3
1.5 2.7 3.14
2.4466666666666668
2
30.42 12
21.21

 */
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        double[] a = new double[n];
        double sum = 0.0;
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextDouble();
            sum += a[i];
        }
        double x = sum / n;
        System.out.println(x);
    }
}
