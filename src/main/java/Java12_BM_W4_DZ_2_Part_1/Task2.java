package Java12_BM_W4_DZ_2_Part_1;

import java.util.Arrays;
import java.util.Scanner;

/*
) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого аналогично передается второй
массив (aj) длины M.
Необходимо вывести на экран true, если два массива одинаковы (то есть
содержат одинаковое количество элементов и для каждого i == j элемент ai ==
aj). Иначе вывести false.
Ограничения:
● 0 < N < 100
● 0 < ai < 1000
● 0 < M < 100
● 0 < aj < 1000
Пример:
Входные данные  Выходные данные
7               true
1 2 3 4 5 6 7
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < a.length; i++) {
            a[i] = sc.nextInt();
        }
        int m = sc.nextInt();
        int[] b = new int[m];
        for (int j = 0; j < b.length; j++) {
            b[j] = sc.nextInt();
        }
        System.out.println(Arrays.equals(a, b));
    }
}
