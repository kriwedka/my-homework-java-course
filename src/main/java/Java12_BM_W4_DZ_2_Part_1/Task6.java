package Java12_BM_W4_DZ_2_Part_1;

import java.util.Scanner;

/*
На вход подается строка S, состоящая только из русских заглавных
букв (без Ё).
Необходимо реализовать метод, который кодирует переданную строку с
помощью азбуки Морзе и затем вывести результат на экран. Отделять коды букв
нужно пробелом.
Для удобства представлен массив с кодами Морзе ниже:
{".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.",
"...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"}
Пример:
Входные данные  Выходные данные
ПРИВЕТ          .--. .-. .. .-- . -
УРА             ..- .-. .-

 */
public class Task6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == 'А') {
                System.out.print(".-" + " ");
            }
            if (word.charAt(i) == 'Б') {
                System.out.print("-..." + " ");
            }
            if (word.charAt(i) == 'В') {
                System.out.print(".--" + " ");
            }
            if (word.charAt(i) == 'Г') {
                System.out.print("--." + " ");
            }
            if (word.charAt(i) == 'Д') {
                System.out.print("-.." + " ");
            }
            if (word.charAt(i) == 'Е') {
                System.out.print("." + " ");
            }
            if (word.charAt(i) == 'Ж') {
                System.out.print("...-" + " ");
            }
            if (word.charAt(i) == 'З') {
                System.out.print("--.." + " ");
            }
            if (word.charAt(i) == 'И') {
                System.out.print(".." + " ");
            }
            if (word.charAt(i) == 'Й') {
                System.out.print(".---" + " ");
            }
            if (word.charAt(i) == 'К') {
                System.out.print("-.-" + " ");
            }
            if (word.charAt(i) == 'Л') {
                System.out.print(".-.." + " ");
            }
            if (word.charAt(i) == 'М') {
                System.out.print("--" + " ");
            }
            if (word.charAt(i) == 'Н') {
                System.out.print("-." + " ");
            }
            if (word.charAt(i) == 'О') {
                System.out.print("---" + " ");
            }
            if (word.charAt(i) == 'П') {
                System.out.print(".--." + " ");
            }
            if (word.charAt(i) == 'Р') {
                System.out.print(".-." + " ");
            }
            if (word.charAt(i) == 'С') {
                System.out.print("..." + " ");
            }
            if (word.charAt(i) == 'Т') {
                System.out.print("-" + " ");
            }
            if (word.charAt(i) == 'У') {
                System.out.print("..-" + " ");
            }
            if (word.charAt(i) == 'Ф') {
                System.out.print("..-." + " ");
            }
            if (word.charAt(i) == 'Х') {
                System.out.print("...." + " ");
            }
            if (word.charAt(i) == 'Ц') {
                System.out.print("-.-." + " ");
            }
            if (word.charAt(i) == 'Ч') {
                System.out.print("---." + " ");
            }
            if (word.charAt(i) == 'Ш') {
                System.out.print("----" + " ");
            }
            if (word.charAt(i) == 'Щ') {
                System.out.print("--.-" + " ");
            }
            if (word.charAt(i) == 'Ъ') {
                System.out.print("--.--" + " ");
            }
            if (word.charAt(i) == 'Ы') {
                System.out.print("-.--" + " ");
            }
            if (word.charAt(i) == 'Ь') {
                System.out.print("-..-" + " ");
            }
            if (word.charAt(i) == 'Э') {
                System.out.print("..-.." + " ");
            }
            if (word.charAt(i) == 'Ю') {
                System.out.print("..--" + " ");
            }
            if (word.charAt(i) == 'Я') {
                System.out.print(".-.-" + " ");
            }
        }
    }

//        Второй вариант решения, боту не подошел.
//        Scanner sc = new Scanner(System.in);
//        String userString = sc.nextLine();
//        System.out.println("");
//        System.out.println(stringConvert(userString));
//    }
//
//
//    public static String stringConvert(String userString) {
//        String currentChar;
//        String getMorseChar;
//        String convertedString = "";
//
//        for (int i = 0; i < userString.length(); i++) {
//            //Get character at i position
//            currentChar = userString.charAt(i) + "";
//
//            //convert character to morse code
//            getMorseChar = convert(currentChar);
//
//            //seperate words with the | symbol
//            if (getMorseChar.equals(" ")) {
//                convertedString = convertedString + "  |  ";
//            } else {
//                //concat the converted letter
//                convertedString = convertedString + getMorseChar;
//
//                //Add a space between each letter
//                if (!getMorseChar.equals(" ")) {
//                    convertedString = convertedString + " ";
//                }
//            }
//        }
//
//        return convertedString;
//
//    }
//    public static String convert (String toEncode) {
//        String morse = toEncode;
//
//        if (toEncode.equalsIgnoreCase("А"))
//            morse = ".-";
//        if (toEncode.equalsIgnoreCase("Б"))
//            morse = "-...";
//        if (toEncode.equalsIgnoreCase("В"))
//            morse = ".--";
//        if (toEncode.equalsIgnoreCase("Г"))
//            morse = "--.";
//        if (toEncode.equalsIgnoreCase("Д"))
//            morse = "-..";
//        if (toEncode.equalsIgnoreCase("Е"))
//            morse = ".";
//        if (toEncode.equalsIgnoreCase("Ж"))
//            morse = "...-";
//        if (toEncode.equalsIgnoreCase("З"))
//            morse = "--..";
//        if (toEncode.equalsIgnoreCase("И"))
//            morse = "..";
//        if (toEncode.equalsIgnoreCase("Й"))
//            morse = ".---";
//        if (toEncode.equalsIgnoreCase("К"))
//            morse = "-.-";
//        if (toEncode.equalsIgnoreCase("Л"))
//            morse = ".-..";
//        if (toEncode.equalsIgnoreCase("М"))
//            morse = "--";
//        if (toEncode.equalsIgnoreCase("Н"))
//            morse = "-.";
//        if (toEncode.equalsIgnoreCase("О"))
//            morse = "---";
//        if (toEncode.equalsIgnoreCase("П"))
//            morse = ".--.";
//        if (toEncode.equalsIgnoreCase("Р"))
//            morse = ".-.";
//        if (toEncode.equalsIgnoreCase("С"))
//            morse = "...";
//        if (toEncode.equalsIgnoreCase("Т"))
//            morse = "-";
//        if (toEncode.equalsIgnoreCase("У"))
//            morse = "..-";
//        if (toEncode.equalsIgnoreCase("Ф"))
//            morse = "..-.";
//        if (toEncode.equalsIgnoreCase("Х"))
//            morse = "....";
//        if (toEncode.equalsIgnoreCase("Ц"))
//            morse = "-.-.";
//        if (toEncode.equalsIgnoreCase("Ч"))
//            morse = "---.";
//        if (toEncode.equalsIgnoreCase("Ш"))
//            morse = "----";
//        if (toEncode.equalsIgnoreCase("Щ"))
//            morse = "--.-";
//        if (toEncode.equalsIgnoreCase("Ъ"))
//            morse = "--.--";
//        if (toEncode.equalsIgnoreCase("Ы"))
//            morse = "-.--";
//        if (toEncode.equalsIgnoreCase("Ь"))
//            morse = "-..-";
//        if (toEncode.equalsIgnoreCase("Э"))
//            morse = "..-..";
//        if (toEncode.equalsIgnoreCase("Ю"))
//            morse = "..--";
//        if (toEncode.equalsIgnoreCase("Я"))
//            morse = ".-.-";
//        return morse;
//    }

//        Третий вариант решения, бот принял:
//        Scanner sc = new Scanner(System.in);
//        String word = sc.nextLine();
//        codeToMorze(word);
//    }
//    public static void codeToMorze(String m) {
//        String s = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
//        String[] a = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.",
//                "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};
//        for (int i = 0; i < a.length; i++) {
//            for (int j = 0; j < m.length(); j++) {
//                if(s.charAt(i) == m.charAt(j)) {
//                    System.out.print(a[i] + " ");
//                }
//            }
//        }
//    }
}
