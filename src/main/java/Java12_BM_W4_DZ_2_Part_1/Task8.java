package Java12_BM_W4_DZ_2_Part_1;

import java.util.Arrays;
import java.util.Scanner;

/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M.
Необходимо найти в массиве число, максимально близкое к M (т.е. такое число,
для которого |ai - M| минимальное). Если их несколько, то вывести
максимальное число.
● 0 < N < 100
● -1000 < ai < 1000
● -1000 < M < 1000
Пример:
Входные данные      Выходные данные
6                   -3
-10 9 -5 -6 1 -3
-4

2                   20
10 20
21

 */
public class Task8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < a.length; i++) {
            a[i] = sc.nextInt();
        }
        int m = sc.nextInt();
        Arrays.sort(a);
        int max = 0;
        for (int i = 0; i < n; i++) {
            if (m > a[i]) {
                max = a[i];
                continue;
            } else if (m < a[i]) {
                max = a[i];
                break;
            }
        }
        System.out.println(max);
    }
}
