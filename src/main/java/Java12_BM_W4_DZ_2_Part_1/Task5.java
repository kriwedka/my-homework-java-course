package Java12_BM_W4_DZ_2_Part_1;

import java.util.Scanner;

/*
 На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M — величина
сдвига.
Необходимо циклически сдвинуть элементы массива на M элементов вправо.
Ограничения:
● 0 < N < 100
● -1000 < ai < 1000
● 0 <= M < 100
Пример:
Входные данные  Выходные данные
5               -11 2 38 44 0
38 44 0 -11 2
2

2               12 15
12 15
0

 */
public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        int m = sc.nextInt();
        int[] b = new int[n];
//        System.arraycopy(a, m + 1, b, 0, a.length - 1);
//        System.arraycopy(a, 0, b, m, a.length - 1 - m);
        for (int i = 0; i < a.length; i++) {
//            if (i < m) {
//                b[i] = a[a.length - 1 - (m - i - 1)];
//            } else {
//                b[i] = a[i - m];
//            }
            b[(i + m) % a.length] = a[i];
        }
        for (int i = 0; i < b.length; i++) {
            System.out.print(b[i] + " ");
        }
    }
}
