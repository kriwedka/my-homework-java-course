package Java12_BM_W4_DZ_2_Part_1;

import java.util.Scanner;

/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.
Ограничения:
● 0 < N < 100
● -1000 < ai < 1000
Пример:
Входные данные  Выходные данные
6               1 9 9 25 64 100
-10 -5 1 3 3 8

2               49 49
-7 7

 */
public class Task7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < a.length; i++) {
            a[i] = sc.nextInt();
        }
        int[] b = new int[n];
        for (int i = 0; i < a.length; i++) {
            b[i] = (int) Math.pow(a[i], 2);
        }
        selectionSort(b);
        for (int i = 0; i < b.length; i++) {
            System.out.print(b[i] + " ");
        }
    }
    public static void selectionSort(int[] b) {
        for (int i = 0; i < b.length - 1; i++) {
            int currentMin = b[i];
            int currentMinIndex = i;

            for (int j = i + 1; j < b.length; j++) {
                if (currentMin > b[j]) {
                    currentMin = b[j];
                    currentMinIndex = j;
                }
            }

            if (currentMinIndex != i) {
                b[currentMinIndex] = b[i];
                b[i] = currentMin;
            }
        }
//        Второй вариант решения:
//        Scanner sc = new Scanner(System.in);
//        int n = sc.nextInt();
//        int[] a = new int[n];
//        int[] b = new int[n];
//        for (int i = 0; i < n; i++) {
//            a[i] = sc.nextInt();
//        }
//
//
//        for (int i = 0; i < n; i++) {
//            b[i] = (int) Math.pow(a[i], 2);
//        }
//        Arrays.sort(b);
//
//        for (int i = 0; i < b.length; i++) {
//            System.out.print(b[i] + " ");
//
//        }
    }
}
