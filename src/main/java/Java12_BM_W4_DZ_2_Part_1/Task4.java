package Java12_BM_W4_DZ_2_Part_1;

import java.util.Scanner;

/*
 На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо вывести на экран построчно сколько встретилось различных
элементов. Каждая строка должна содержать количество элементов и сам
элемент через пробел.
Ограничения:
● 0 < N < 100
● -1000 < ai < 1000
Пример:
Входные данные  Выходные данные
6               3 7
7 7 7 10 26 26  1 10
                2 26

2               1 -5
-5 7            1 7

 */
public class Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < a.length; i++) {
            a[i] = sc.nextInt();
        }
        sc.close();
        amountOfElements(a);
    }
    public static void amountOfElements(int[] Array) {
        int count = 1;
        for (int a = 0; a < Array.length; a++) {
            for (int j = 1; j < Array.length; j++) {
                if (Array[a] == Array[j]) {
                    count++;
                }
            }
            System.out.println(count + " " + Array[a]);
            int[] Array1 = new int[Array.length - count];
            System.arraycopy(Array, count, Array1, 0, Array.length - count);
            if (Array.length == count) {
                System.exit(0);
            } else {
                amountOfElements(Array1);
            }
        }
    }
}
