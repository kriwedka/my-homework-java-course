package Java12_BM_W4_DZ_2_Part_1;

import java.util.Scanner;

/*
На вход подается число N — длина массива. Затем передается массив
строк из N элементов (разделение через перевод строки). Каждая строка
содержит только строчные символы латинского алфавита.
Необходимо найти и вывести дубликат на экран. Гарантируется что он есть и
только один.
Ограничения:
● 0 < N < 100
● 0 < ai.length() < 1000
Пример:
Входные данные  Выходные данные
4               java
hello
java
hi
java

7               most
today
is
the
most
most
special
day

 */
public class Task9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String[] inputStrings = new String[n];
        for (int i = 0; i < inputStrings.length; i++) {
            inputStrings[i] = sc.next();
        }
        String s = "";
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    if (inputStrings[i].equals(inputStrings[j])) {
                        s = inputStrings[i];
                    }
                }
            }
        }
        System.out.println(s);
    }
}
