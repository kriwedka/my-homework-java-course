package Java12_BM_W4_DZ_2_Part_1;

import java.util.Scanner;

/*
(1 балл) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию. После этого
вводится число X — элемент, который нужно добавить в массив, чтобы
сортировка в массиве сохранилась.
Необходимо вывести на экран индекс элемента массива, куда нужно добавить
X. Если в массиве уже есть число равное X, то X нужно поставить после уже
существующего.
Ограничения:
● 0 < N < 100
● -1000 < ai < 1000
● -1000 < X < 1000
Пример:
Входные данные      Выходные данные
6                   1
10 20 30 40 45 60
12

5                   4
-1 0 2 2 3
2
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        int x = sc.nextInt();
        System.out.println(linearSearch(a , x));
    }

    public static int linearSearch(int[] a, int x) {
        int num = 0;
        for (int i = 0; i < a.length; i++) {
            if (x < a[i]) {
                num = i;
                break;
            }
        }
        return num;
    }
}

