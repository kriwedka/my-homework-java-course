package Java12_BM_W6_DZ_3_Part_1.Task_5;

import java.util.ArrayList;
import java.util.List;

/*
Необходимо реализовать класс DayOfWeek для хранения порядкового номера
дня недели (byte) и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek
длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…
7 Sunday
 */
public class Main {
    public static void main(String[] args) {
        List<DayOfWeek> dayOfWeeks = new ArrayList<>();
        dayOfWeeks.add(new DayOfWeek((byte) 1,"Monday"));
        dayOfWeeks.add(new DayOfWeek((byte) 2,"Tuesday"));
        dayOfWeeks.add(new DayOfWeek((byte) 3,"Wednesday"));
        dayOfWeeks.add(new DayOfWeek((byte) 4,"Thursday"));
        dayOfWeeks.add(new DayOfWeek((byte) 5,"Friday"));
        dayOfWeeks.add(new DayOfWeek((byte) 6,"Saturday"));
        dayOfWeeks.add(new DayOfWeek((byte) 7,"Sunday"));
        for (DayOfWeek dayOfWeek : dayOfWeeks) {
            System.out.println(dayOfWeek.getDayId() + " " + dayOfWeek.getDayName());
        }
    }
}


