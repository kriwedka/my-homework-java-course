package Java12_BM_W6_DZ_3_Part_1.Task_5;

public class DayOfWeek {
    private byte dayId;
    private String dayName;

    DayOfWeek(byte dayId, String dayName) {
        this.dayId = dayId;
        this.dayName = dayName;

    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayId(byte dayId) {
        this.dayId = dayId;
    }

    public byte getDayId() {
        return dayId;
    }
}
