package Java12_BM_W6_DZ_3_Part_1.Task_6;
/*
Необходимо реализовать класс AmazingString, который хранит внутри себя
строку как массив char и предоставляет следующий функционал:
Конструкторы:
● Создание AmazingString, принимая на вход массив char
● Создание AmazingString, принимая на вход String
Публичные методы (названия методов, входные и выходные параметры
продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е.
не прибегая к переводу массива char в String и без использования стандартных
методов класса String.
● Вернуть i-ый символ строки
● Вернуть длину строки
● Вывести строку на экран
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается массив char). Вернуть true, если найдена и false иначе
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается String). Вернуть true, если найдена и false иначе
● Удалить из строки AmazingString ведущие пробельные символы, если
они есть
● Развернуть строку (первый символ должен стать последним, а
последний первым и т.д.)
 */
public class Main {
    public static void main(String[] args) {
        AmazingString amazingString = new AmazingString("Hello, how are you");
        AmazingString amazingString1 = new AmazingString(new char[]{'H', 'e', 'l', 'l', 'o'});
        amazingString.returnElementArr(4);
        System.out.println(amazingString.lengthArr());
        amazingString.printArr();
        amazingString.printArr();
        amazingString.reversPrinArr();
        amazingString.removSpaceArr();
        amazingString1.newArrEqualCharrArr(new char[]{'H', 'e', 'l', 'l', 'o'});
        amazingString.removSpaceArr();
        amazingString.newArrEqualCharrArr(new char[]{'a', 'r', 'e'});
        amazingString.newStringEqualCharArr("are");
    }
}

