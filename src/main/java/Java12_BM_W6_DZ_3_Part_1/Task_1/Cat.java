package Java12_BM_W6_DZ_3_Part_1.Task_1;

import java.util.Random;

public class Cat {

    private static final int NUMBER_FUNCTION = 3;

    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");
    }

    private void eat() {
        System.out.println("Eat");
    }

    public void status() {
        Random x = new Random();
        switch (x.nextInt(NUMBER_FUNCTION)) {
            case 0:
                sleep();
                break;
            case 1:
                meow();
                break;
            case 2:
                eat();
                break;

        }
    }
}
