package Java12_BM_W6_DZ_3_Part_1.Task_2_and_3;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Student person = new Student();
        Student person1 = new Student();
        Student person2 = new Student();
        Student[] students = {person, person1, person2};
        person2.setName("Sergey");
        person2.setSurname("Sergeev");
        person2.setGrages(new int[]{5, 3, 9, 9, 8, 3, 6, 7, 8, 9});

        person1.setName("Petr");
        person1.setSurname("Petrov");
        person1.setGrages(new int[]{5, 3, 4, 6, 8, 3, 6, 7, 8, 9});

        person.setName("Ivan");
        person.setSurname("Ivanov");
        person.setGrages(new int[]{4, 5, 1, 2, 7, 7, 7, 5, 4, 3});

        System.out.println(person.getName() + " " + person.getSurname());
        System.out.println(Arrays.toString(person.getGrages()));//для проверки двигается ли влево массив
        System.out.println(person.averageScore() + " " + person.getName() + " " + person.getSurname());//средний бал
        System.out.println(person1.averageScore() + " " + person1.getName() + " " + person1.getSurname());
        System.out.println(person2.averageScore() + " " + person2.getName() + " " + person2.getSurname());
        System.out.println(StudentService.bestStudent(students).getName() + " "
                + StudentService.bestStudent(students).getSurname());// студент с лучшим средним баллом
        System.out.println(students[1].getSurname());
        StudentService.sortBySurname(students);
        System.out.println(Arrays.toString(students));
        System.out.println(students[0].getName() + " " + students[0].getSurname());
        System.out.println(students[1].getName() + " " + students[1].getSurname());
        System.out.println(students[2].getName() + " " + students[2].getSurname());
    }
}

