package Java12_BM_W6_DZ_3_Part_1.Task_2_and_3;
/*
Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но
не может быть больше 10.
И следующие публичные методы:
● геттер/сеттер для name
● геттер/сеттер для surname
● геттер/сеттер для grades
● метод, добавляющий новую оценку в grades. Самая первая оценка
должна быть удалена, новая должна сохраниться в конце массива (т.е.
массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)
 */
public class Student {
    private String name;
    private String surname;
    private int[] grages = new int[10];

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setGrages(int[] grages) {
        this.grages = grages;
    }

    public int[] getGrages() {
        return grages;
    }

    public void newGrages(int ocenka) {//метод, добавляющий новую оценку в grades
        for (int i = 0; i < grages.length - 1; i++) {
            grages[i] = grages[i + 1];
        }
        grages[grages.length - 1] = ocenka;
    }

    public double averageScore() {//метод, возвращающий средний балл студента
        double x = 0;
        int a = 0;
        for (int i = 0; i < grages.length; i++) {
            if (grages[i] > 0) {
                x += grages[i];
                a++;
            }
        }
        return x = x / a;
    }
}

