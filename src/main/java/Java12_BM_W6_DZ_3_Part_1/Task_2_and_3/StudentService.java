package Java12_BM_W6_DZ_3_Part_1.Task_2_and_3;
/*
Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
● bestStudent() — принимает массив студентов (класс Student из
предыдущего задания), возвращает лучшего студента (т.е. который
имеет самый высокий средний балл). Если таких несколько — вывести
любого.
● sortBySurname() — принимает массив студентов (класс Student из
предыдущего задания) и сортирует его по фамилии.

 */
public class StudentService {

    public static Student bestStudent(Student[] list) {
        int x = 0;
        double a = list[0].averageScore();
        for (int i = 0; i < list.length - 1; i++) {
            if (a < list[i + 1].averageScore()) {
                x = i + 1;
                a = list[i + 1].averageScore();
            }
        }
        return list[x];

    }/* sortBySurname() — принимает массив студентов (класс Student из
       предыдущего задания) и сортирует его по фамилии.
       */

    public static void sortBySurname(Student[] student) {

        Student x;
        for (int i = 0; i < student.length; i++) {
            char a = student[i].getSurname().charAt(0);
            int l = i;

            for (int j = i + 1; j < student.length; j++) {
                if (a > student[j].getSurname().charAt(0)) {
                    x = student[i];
                    student[i] = student[j];
                    student[j] = x;
                }


            }
        }

    }
}

