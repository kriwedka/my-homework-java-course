package Java12_BM_W6_DZ_3_Part_1.Task_8;

public class Atm {
    static int countInstans;                //Переменная счетчик для подсчета созданных инстансов
    private double dollarExchangeRate;
    private double rubelsExchangeRate;

    public Atm(double dollarExchangeRate , double rubelsExchangeRate){  //Создаем конструктор в котором задаем значения ждя курса валют
        if (dollarExchangeRate > 0 && rubelsExchangeRate > 0){          //Проверяем чтобы значение было положительное
            this.dollarExchangeRate = dollarExchangeRate;
            this.rubelsExchangeRate = rubelsExchangeRate;
        }else {
            System.out.println("Enter a positive value");
        }
        countInstans++;
    }


    public void converDollarsInRubels(double dollars){  //Переводим доллары в рубли по курсу доллара
        System.out.println(dollars * this.dollarExchangeRate);
    }

    public void converRubelsInDollars(double rubels){ //Переводим рубли в доллары в рубли по курсу рублей
        System.out.println(rubels * rubelsExchangeRate);
    }

    public static int returCountInstans(){   //Возвращаем значение созданных инстансов
        return countInstans;
    }

}

