package Java12_BM_W6_DZ_3_Part_1.Task_4;

public class TimeUnit {
    private final int SECONDS_IN_MINUTES = 60;
    private final int MINUTES_IN_HOURS = 60;
    private  final int HOURS_IN_DAY = 24;
    private int minutes;
    private int hours;
    private int seconds;


    TimeUnit(int hours, int minutes, int seconds) {
        this.hours = (hours >= 0 && hours <= 23 ? hours : 0);
        this.minutes = (minutes >= 0 && minutes <= 59 ? minutes : 0);
        this.seconds = (seconds >= 0 && seconds <= 59 ? seconds : 0);

    }

    TimeUnit(int hours, int minutes) {
        this.hours = (hours >= 0 && hours <= 23 ? hours : 0);
        this.minutes = (minutes >= 0 && minutes <= 59 ? minutes : 0);
        this.seconds = 0;
    }

    TimeUnit(int hours) {
        this.hours = (hours >= 0 && hours <= 23 ? hours : 0);
        this.minutes = 0;
        this.seconds = 0;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = (hours >= 0 && hours <= 23 ? hours : 0);
        if (hours < 0 || hours > 23) {
            System.out.println("You entered an incorrect number, the value of hours is 0");
        }
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = (minutes >= 0 && minutes <= 59 ? minutes : 0);
        if (minutes < 0 || minutes > 59) {
            System.out.println("You entered an incorrect number, the minutes value is 0");
        }
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = (seconds >= 0 && seconds <= 59 ? seconds : 0);
        if (seconds < 0 || seconds > 59) {
            System.out.println("You entered an incorrect number, the seconds value is 0");
        }
    }
    //Выводим время  в формате hh:mm:ss
    public void setTime() {
        System.out.printf("%02d:%02d:%02d \n", this.hours, this.minutes, this.seconds);
    }
    //Выводим время  в формате hh:mm:ss am/pm
    public void timeMeridian() {
        if (this.hours > 12) {
            System.out.printf("%02d:%02d:%02d pm \n", this.hours - 12, this.minutes, this.seconds);
        } else {
            System.out.printf("%02d:%02d:%02d am \n", this.hours, this.minutes, this.seconds);
        }
    }
    //Прибавляем переданное время к установленному
    public void sumTimeUnit(int hours, int minutes, int seconds){
        this.seconds = this.seconds + seconds;
        this.minutes = this.minutes + minutes;
        this.hours = this.hours + hours;
        if (this.seconds >= SECONDS_IN_MINUTES){
            this.minutes += this.seconds / SECONDS_IN_MINUTES;
            this.seconds = this.seconds % SECONDS_IN_MINUTES;
        }
        if (this.minutes >= MINUTES_IN_HOURS){
            this.hours += this.minutes / MINUTES_IN_HOURS;
            this.minutes = this.minutes % MINUTES_IN_HOURS;
        }
        if (this.hours >= HOURS_IN_DAY){
            this.hours = this.minutes % HOURS_IN_DAY;
        }
    }
}


