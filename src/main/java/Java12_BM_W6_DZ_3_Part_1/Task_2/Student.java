package Java12_BM_W6_DZ_3_Part_1.Task_2;
/*
Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но
не может быть больше 10.
И следующие публичные методы:
● геттер/сеттер для name
● геттер/сеттер для surname
● геттер/сеттер для grades
● метод, добавляющий новую оценку в grades. Самая первая оценка
должна быть удалена, новая должна сохраниться в конце массива (т.е.
массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)
 */
import java.util.Comparator;

public class Student { //Создаем класс Student
    private static final int ARRAY_SIZE = 10; //Максимальный размер массива который может передаваться в класс
    private String name;
    private String surname;
    private int[] grades;

    Student(String name, String surname, int[] grades) {
        this.name = name;
        this.surname = surname;
        if (grades.length <= 10) {   //Проверяю входящий массив оценок , если он больше 10 то записываем только последнии 10 значений
            this.grades = grades;
        } else {
            this.grades = new int[ARRAY_SIZE];
            int count = 1;
            for (int i = 9; i >= 0; i--) {
                this.grades[i] = grades[grades.length - count];
                count++;
            }

        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {
        return grades;
    }

    void setGrades(int[] grades) {     //При вводе нового массива так же делаю проверку на его размер
        if (grades.length <= 10) {     //Но я не уверен можно ли так делать
            this.grades = grades;
        } else {
            this.grades = new int[ARRAY_SIZE];
            int count = 1;
            for (int i = 9; i >= 0; i--) {
                this.grades[i] = grades[grades.length - count];
                count++;
            }
        }
    }


    public double average() {  //Получаем средний балл студента
        double average = 0;
        for (int i = 0; i < grades.length; i++) {
            average += grades[i];
        }
        return average / grades.length;
    }

    public void newGrade(int grade) {                  //Добавляем новую оценку
        if (grades.length < ARRAY_SIZE) {              //Если длина массива меньше максимальной длины
            int[] arr = new int[this.grades.length + 1];    // то создаем новый массив длиннее на 1 ячейку
            for (int i = 0; i < arr.length; i++) {
                if (i == arr.length - 1) {
                    arr[i] = grade;
                } else arr[i] = grades[i];
            }
            this.grades = arr;
        } else {
            for (int i = 0; i < grades.length; i++) {
                if (i == grades.length - 1) {
                    this.grades[i] = grade;
                } else {
                    this.grades[i] = grades[i + 1];
                }

            }
        }
    }

    public static class SureNameComparator implements Comparator<Student> { // Создаю Компаратор для возможности сортировать фамилий
        @Override                                                           // с помощью метода collection.sort
        public int compare(Student o1, Student o2) {
            return o1.getSurname().compareTo(o2.getSurname());
        }
    }
}

