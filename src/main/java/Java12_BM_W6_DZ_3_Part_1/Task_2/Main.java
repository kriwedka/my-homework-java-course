package Java12_BM_W6_DZ_3_Part_1.Task_2;

import Java12_BM_W6_DZ_3_Part_1.Task_3.StudentService;

import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        Student student = new Student("Maxim", "Maximov", new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13});
        Student student1 = new Student("Gleb", "Glebov", new int[]{12, 10, 3, 4, 17, 2, 15, 25, 18, 21});
        Student student2 = new Student("Ivan", "Ivanov", new int[]{13, 15, 20, 19, 18, 23, 18, 2, 3, 5, 45, 23});
        Student student3 = new Student("Petr" , "Petrov" , new int[]{5 , 6 , 8} );
        students.add(student);
        students.add(student1);
        students.add(student2);
        students.add(student3);
        System.out.println(student.average());
        System.out.println(student1.average());
        System.out.println(student2.average());
        System.out.println(student3.average());
        student3.newGrade(19);
        student2.newGrade(21);
        StudentService.bestStudent(students);
        StudentService.sortBySurename(students);


        for (int i = 0; i < 4; i++) {
            System.out.print(students.get(i) + " ");

        }
    }
}
