package Java12_BM_W6_DZ_3_Part_1.Task_7;

public class TriangleChecker {
    public static boolean makeTriangle(double a , double b, double c){
        return a < b + c && b < a + c && c < a + b;
    }
}

