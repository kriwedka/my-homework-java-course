package Java12_BM_W6_DZ_3_Part_1.Task_7;
/*
Реализовать класс TriangleChecker, статический метод которого принимает три
длины сторон треугольника и возвращает true, если возможно составить из них
треугольник, иначе false. Входные длины сторон треугольника — числа типа
double. Придумать и написать в методе main несколько тестов для проверки
работоспособности класса (минимум один тест на результат true и один на
результат false)
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(TriangleChecker.makeTriangle(12.5, 5.6, 7.8));
        System.out.println(TriangleChecker.makeTriangle(6.8, 7.4, 17.1));
    }
}

