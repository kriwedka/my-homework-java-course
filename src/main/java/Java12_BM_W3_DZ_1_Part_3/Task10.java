package Java12_BM_W3_DZ_1_Part_3;
/*
 Вывести на экран “ёлочку” из символа звездочки (*) заданной высоты N. На N +
1 строке у “ёлочки” должен быть отображен ствол из символа |
Ограничения:
2 < n < 10
 */
import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int size = sc.nextInt();

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < i * 2 + 1; j++){
                if (i == 0){
                    for (int tip = 0; tip < (((size * 2) / 2) - 1); tip++){
                        System.out.print(" ");
                    }
                    System.out.print("#");
                }
                else {
                    System.out.print("#");
                }
            }
            System.out.println("");
            for (int x = i; x < (size - 2); ++x) {
                System.out.print(" ");
            }
        }
        for (int stump = 0; stump < (((size * 2) / 2) - 1); stump++) {
            System.out.print(" ");
        }
        System.out.print("|");
    }
}
