package Java12_BM_W3_DZ_1_Part_3;
/*
for  (здесь прямая зависимость от введенного n ) {ввод числа(int m = scanner.nextInt())
if(число введенное сравниваем с p){складываем если подходит}}
 */
import java.util.Scanner;

/*
На вход подается:
○ целое число n,
○ целое число p
○ целые числа a1, a2 , … an
Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго
больше p.
Ограничения:
0 < m, n, ai < 1000
Пример:
Входные данные   Выходные данные
2                126
18
95 31

6                326
29
40 37 97 72 80 18

1                0
100
42


 */
public class Task8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int p = sc.nextInt();
        int sum = 0;
        for (int i = 0; i < n; i++) {
            int a = sc.nextInt();
            if (a > p) {
                sum = sum + a;
            }
        }
        System.out.println(sum);
    }
}
