package Java12_BM_W3_DZ_1_Part_3;

import java.util.Scanner;

/*
Дана строка s. Вычислить количество символов в ней, не считая пробелов
(необходимо использовать цикл).
Ограничения:
0 < s.length() < 1000
Пример:
Входные данные Выходные данные
Hello world    10
Never give up  11
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        int res = 0;
        for(int i = 0; i < s.length(); i++) {
            if(s.charAt(i) != ' ') {
                res++;
            }
        }
        System.out.println(res);
    }
}
