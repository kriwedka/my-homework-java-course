package Java12_BM_W3_DZ_1_Part_3;

import java.util.Scanner;

/*
Дано натуральное число n. Вывести его цифры в “столбик”.
Ограничения:
0 < n < 1000000
Пример:
Входные данные Выходные данные
74              7
                4
1630            1
                6
                3
                0

 */
public class Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
//        String n = scanner.nextLine();
//        for (int i = 0; i < n.length(); ++i) {
//            System.out.println(n.charAt(i));
//        }
//        if(n>=10_000) {
//            System.out.println(m=n/10_000);
//            flag = true;
//            n -= m*10_000;
//        } else {
//            if (flag) {
//                System.out.println(0);
//            }
//        }

        int n = sc.nextInt();
        int m = 0;
        if(n>=100000) {
            System.out.println(m=n/100000);
            n -= m*100000;
        }
        if(n>=10000) {
            System.out.println(m=n/10000);
            n -= m*10000;
        }
        if(n>=1000) {
            System.out.println(m=n/1000);
            n -= m*1000;
        }
        if(n>=100) {
            System.out.println(m=n/100);
            n -= m*100;
        }
        if(n>=10) {
            System.out.println(m=n/10);
            n -= m*10;
        }
        if(n>=0) {
            System.out.println(m=n/1);
            n -= m;
        }
    }
}
