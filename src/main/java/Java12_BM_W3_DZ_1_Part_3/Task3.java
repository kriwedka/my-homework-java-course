package Java12_BM_W3_DZ_1_Part_3;

import java.util.Scanner;

/*
На вход подается два положительных числа m и n. Необходимо вычислить m^1
+ m^2 + ... + m^n
Ограничения:
0 < m, n < 10
Пример:
Входные данные Выходные данные
1 1             1
8 5             37448
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        int res = 0;
        for (int i = 1; i <= n; i++) {
            res += Math.pow(m, i);
        }
        System.out.println(res);
    }
}
