package Java12_BM_W3_DZ_1_Part_3;
/*
На вход подается два положительных числа m и n. Найти сумму чисел между m
и n включительно.
Ограничения:
0 < m, n < 10
m < n
Пример:
Входные данные Выходные данные
7 9             24
1 2             3
 */
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        int res = 0;
        for (int i = m; i <= n; i++) {
            res = res + i;
        }
        System.out.println(res);
    }
}
