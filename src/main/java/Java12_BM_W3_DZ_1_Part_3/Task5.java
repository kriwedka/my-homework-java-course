package Java12_BM_W3_DZ_1_Part_3;

import java.util.Scanner;

/*
Даны положительные натуральные числа m и n. Найти остаток от деления m на
n, не выполняя операцию взятия остатка.
Ограничения:
0 < m, n < 10
Пример:
Входные данные Выходные данные
9 1             0
8 3             2
7 9             7
 */
public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        int res = m - (m / n) * n;
        System.out.println(res);
    }
}

