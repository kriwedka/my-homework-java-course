/*
Ирина, подруга Пети, решила создать свой бизнес по продаже цветов. Начать
она решила с самых основ: создать соответствующую базу данных для своего
бизнеса. Она точно знает, что будет продавать Розы по 100 золотых монет за
единицу, Лилии по 50 и Ромашки по 25.
Помимо этого, ей хочется хранить данные своих покупателей (естественно они
дали согласие на хранение персональной информации). Сохранять нужно Имя
и Номер телефона.
И, конечно, данные самого заказа тоже нужно как-то хранить! Ирина пока не
продумала поля, но она точно хочет следовать следующим правилам:
● в рамках одного заказа будет продавать только один вид цветов
(например, только розы)
● в рамках одного заказа можно купить от 1 до 1000 единиц цветов.
А также она представляет, какие запросы хочет делать к базе:
1. По идентификатору заказа получить данные заказа и данные клиента,
создавшего этот заказ
2. Получить данные всех заказов одного клиента по идентификатору
клиента за последний месяц
3. Найти заказ с максимальным количеством купленных цветов, вывести их
название и количество
4. Вывести общую выручку (сумму золотых монет по всем заказам) за все
время
Выполненное домашнее задание должно содержать:
1. Sql-скрипты создания таблиц с необходимыми связями
2. Sql-скрипты наполнения таблиц (тестово добавить несколько строк данных для
последующей проверки select’ов)
3. Sql-скрипты получения данных в соответствии с условием
*/

create table products(
    id_product  serial primary key,
    title       varchar(50) not null,
    price       decimal(18,0) not null,
    date_added  timestamp not null
)

insert into products(title, price, date_added) values ('Розы', '100', now());
insert into products(title, price, date_added) values ('Лилии', '50', now());
insert into products(title, price, date_added) values ('Ромашки', '25', now());

commit;

create table customers(
    id_customer     serial primary key,
    name_customer   varchar(100) not null,
    phone_number    varchar(100) not null
)
insert into customers(name_customer, phone_number) values ('Иванов И.И.', '+7-976-846-24-65');
insert into customers(name_customer, phone_number) values ('Петров П.П.', '+7-986-880-23-95');
insert into customers(name_customer, phone_number) values ('Машков М.М.', '+7-954-345-94-01');

create table orders(
    id_order            serial primary key,
    id_product          decimal(18,0) not null,
    id_customer         decimal(18,0) not null,
    quantity_of_product decimal(18,0) not null,
    date_added          timestamp not null
)

insert into orders(id_product, id_customer, quantity_of_product, date_added) values ('2', '2', '25', '03/03/2022 02:03:04');
insert into orders(id_product, id_customer, quantity_of_product, date_added) values ('2', '3', '31', '10/12/2022 04:24:00');
insert into orders(id_product, id_customer, quantity_of_product, date_added) values ('4', '2', '50', '05/09/2022 16:03:00');
insert into orders(id_product, id_customer, quantity_of_product, date_added) values ('3', '1', '9', '03/08/2022 08:10:00');
insert into orders(id_product, id_customer, quantity_of_product, date_added) values ('3', '3', '13', '10/10/2022 10:14:00');
insert into orders(id_product, id_customer, quantity_of_product, date_added) values ('4', '1', '55', '06/06/2022 20:00:00');
insert into orders(id_product, id_customer, quantity_of_product, date_added) values ('2', '1', '11', '01/01/2023 17:00:00');
insert into orders(id_product, id_customer, quantity_of_product, date_added) values ('4', '1', '33', '08/01/2023 15:25:00');
insert into orders(id_product, id_customer, quantity_of_product, date_added) values ('3', '2', '7', '12/01/2023 09:50:00');

--1. По идентификатору заказа получить данные заказа и данные клиента,
--создавшего этот заказ
select * from orders t1
join customers t2 on t1.id_customer = cast(t2.id_customer as decimal(18,0));

--2. Получить данные всех заказов одного клиента по идентификатору
--клиента за последний месяц
select * from orders
where id_customer = '1' and date_added > current_date - interval '1 months';

--3. Найти заказ с максимальным количеством купленных цветов, вывести их
--название и количество
select * from orders
where quantity_of_product = (select max(quantity_of_product) from orders);

--4. Вывести общую выручку (сумму золотых монет по всем заказам) за все
--время
select sum(t2.price * t1.quantity_of_product) as summary_income from orders t1
join products t2 on t1.id_product = cast(t2.id_product as decimal(18,0))
