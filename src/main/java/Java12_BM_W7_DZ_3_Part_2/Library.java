package Java12_BM_W7_DZ_3_Part_2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Library {
    final String BOOK_AVAILABLE = "Available for issue.";
    final String BOOK_UNAVAILABLE = "The book has already been taken.";
    private final List<Book> books = new ArrayList<>(); //Массив книг
    private final List<Person> persons = new ArrayList<>();//Массив зарегистрированных читателей


    public void addBook(String bookAuthor, String nameBook) { //Добавляем книгу в библиотеку
        Book book = new Book(bookAuthor, nameBook);
        if ((books).size() == 0) {         // Если книг в библиотеке еше нет то добавляем
            book.setBookStatus(BOOK_AVAILABLE);  //Для добавленной книги уставнавливаем статус , что книга доступна
            books.add(book);
        } else {
            for (Book p : books) {
                if (p.getNameBook().equalsIgnoreCase(book.getNameBook())) { //Проверяем есть ли уже такая книга в библиотеке
                    System.out.println("This book is already there.");
                    break;
                } else {
                    books.add(book);
                    book.setBookStatus(BOOK_AVAILABLE);
                    break;
                }
            }
        }
    }

    public void deleteBook(String nameBook) {// Удаляем книгу из библиотеки
        Iterator<Book> iterator = books.iterator();
        while (iterator.hasNext()) {
            Book book = iterator.next();
            if (book.getNameBook().equalsIgnoreCase(nameBook) && book.getBookStatus().equals(BOOK_AVAILABLE)) {
                iterator.remove(); // Удаляем книгу если она находится в библиотеке и доступна для выдачи
            }
        }
    }

    public void searchBooksAuthor(String authorName) { // Ищем все книги автора которые есть в библиотеке
        for (Book book : books) {
            if (book.getBookAuthor().equalsIgnoreCase(authorName)) {
                System.out.println(book.getNameBook());
            }
        }
    }

    public Book getSearchBooks(String bookName) {  // Ищем книгу по названию и возвращаем ее
        Book searchBook = null;
        for (Book book : books) {
            if (book.getNameBook().equalsIgnoreCase(bookName)) {
                searchBook = book;
            }

        }
        return searchBook;
    }

    public void newPerson(String name, String surname) { // Добавляем нового пользователя , и присваиваем ему персональный номер
        Person person = new Person(name, surname);
        persons.add(person);
        System.out.println("Your personal number: " + person.getPersonalId());
    }

    public void newPerson(String name, String surname, String bookName) { //Добавляем нового пользователя, и можем ему передать книгу
        Person person = new Person(name, surname);    // Если она доступна
        persons.add(person);   //Добавляем пользователя в библиотеку , вне зависимости от результата
        for (Book book : books) {
            if (book.getNameBook().equalsIgnoreCase(bookName)) {
                if (book.getBookStatus().equals(BOOK_AVAILABLE)) {
                    book.setBookStatus(BOOK_UNAVAILABLE);
                    person.setBookPerson(book.getNameBook());
                    System.out.println("Your personal number: " + person.getPersonalId());
                } else {
                    System.out.println("This book is busy. " + "You have been assigned a personal number: " + person.getPersonalId());
                }
            }
        }
    }

    public void personTakeBook(int personalId, String bookName) { //Читатель берет книгу , по своему персональному номеру
        if (persons.get(personalId).getBookPerson() == null) { //Проверяем есть ли у пользователя книга
            for (Book book : books) {
                if (book.getNameBook().equalsIgnoreCase(bookName)) {
                    if (book.getBookStatus().equals(BOOK_AVAILABLE)) { //Если книга доступна для выдачи
                        persons.get(personalId).setBookPerson(book.getNameBook()); //Читателю присваиваем название книги
                        book.setBookStatus(BOOK_UNAVAILABLE);  // У книги меняем статус
                    } else {
                        System.out.println("This book is busy. Take another one.");
                        break;
                    }
                }
            }
        } else
            System.out.println("You already have a book.");
    }

    public void returnBookInLibrary(int personalId, String bookName) { // Читатель возвращает книгу в библиотеку
        if (persons.get(personalId).getBookPerson().equalsIgnoreCase(bookName)) { //Смотрим есть ли у читателя эта книга
            persons.get(personalId).setBookPerson(null); //Устанавливаем значение null для читателя
            for (Book book : books) {
                if (book.getNameBook().equalsIgnoreCase(bookName)) {
                    book.setBookStatus(BOOK_AVAILABLE);//Книга снова доступна для выдачи
                }

            }
        } else {
            System.out.println("You didn't take this book.");
        }
    }

    public void returnBookInLibrary(int personalId, String bookName, int grade) { //Читатель дополнительно может оценить книгу

        if (persons.get(personalId).getBookPerson().equalsIgnoreCase(bookName)) {
            persons.get(personalId).setBookPerson(null);
            for (Book book : books) {
                if (book.getNameBook().equalsIgnoreCase(bookName)) {
                    book.setBookStatus(BOOK_AVAILABLE);
                    book.addGrade(grade);
                }

            }
        } else {
            System.out.println("You didn't take this book.");
        }
    }

    // Методы для тестов, верно ли отображаются значения
    public void statusBook() {
        for (Book book : books) {
            System.out.println(book.getBookAuthor() + " " + book.getNameBook() + " " + book.getBookStatus() + " " + book.getAverageGrade());

        }

    }

    public void statusPerson() {
        for (Person person : persons) {
            System.out.println(person.getName() + " " + person.getSurname() + " " + person.getPersonalId() + " " + person.getBookPerson());

        }

    }
}

