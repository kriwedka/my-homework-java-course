package Java12_BM_W7_DZ_3_Part_2;
/*
Итак, Петя решил открыть библиотеку… Но ему нужна ваша помощь!
Должен быть реализован класс Книга, содержащий название и автора. Также должен
быть реализован класс Посетитель, содержащий имя посетителя и идентификатор
(null до тех пор, пока не возьмет книгу). Должен быть реализован класс Библиотека со
следующим функционалом:
Работа со списком существующих книг в библиотеке.
Сюда входят все добавленные книги, в том числе и одолженные. Название книги
считается уникальным, и в библиотеке не может быть двух книг с одинаковым
названием.
1. Добавить новую книгу в библиотеку, если книги с таким наименованием ещё нет
в библиотеке. Если книга в настоящий момент одолжена, то считается, что она
всё равно есть в библиотеке (просто в настоящий момент недоступна).
2. Удалить книгу из библиотеки по названию, если такая книга в принципе есть в
библиотеке и она в настоящий момент не одолжена.
3. Найти и вернуть книгу по названию.
4. Найти и вернуть список книг по автору.
 */
import java.util.List;
public class Main {
    private static List<Book> books;
    private List<Person> persons;

    public static void main(String[] args) {
        Library library = new Library();
        library.addBook("Tolstoy L.N.", "War and peace.");
        library.addBook("Strugatsky A.N.", "A roadside picnic.");
        library.addBook("Tolstoy L.N.", "War and peace.");
        library.addBook("Perumov N.", "The death of the gods.");
        library.addBook("Perumov N.", "The war of the magician.");
        library.addBook("Perumov N.", "The skull on the sleeve.");
        library.statusBook();
        System.out.println();

        library.deleteBook("War and peace.");
        library.statusBook();
        System.out.println();

        library.searchBooksAuthor("Perumov N.");
        System.out.println();

        library.newPerson("Sasha", "Petrov");
        library.newPerson("Alexey", "Naryshkin");
        library.newPerson("Peter", "Sloboda");
        library.newPerson("Dmitry", "Skvortsov", "A roadside picnic.");
        library.newPerson("Ivan", "Ivanov", "A roadside picnic.");

        library.personTakeBook(0, "The skull on the sleeve.");
        library.personTakeBook(2, "A roadside picnic.");
        library.personTakeBook(3, "The skull on the sleeve.");
        library.personTakeBook(3, "A roadside picnic.");

        library.returnBookInLibrary(3, "A roadside picnic.", 7);
        library.personTakeBook(3, "A roadside picnic.");
        library.returnBookInLibrary(3, "Life.", 2);
        library.personTakeBook(3, "A roadside picnic.");
        System.out.println();
        library.statusPerson();
        System.out.println();
        library.statusBook();

    }
}
