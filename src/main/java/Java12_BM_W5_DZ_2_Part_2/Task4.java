package Java12_BM_W5_DZ_2_Part_2;

import java.util.Scanner;

/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передается сама матрица, состоящая из натуральных чисел. После этого
передается натуральное число P.
Необходимо найти элемент P в матрице и удалить столбец и строку его
содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
Гарантируется, что искомый элемент единственный в массиве.
Ограничения:
● 0 < N < 100
● 0 < ai < 1000
Пример:
Входные данные  Выходные данные
3               1 3
1 2 3           1 3
1 7 3
1 2 3
7

4               1 2 3
1 2 3 4         1 2 3
1 2 3 4         1 2 3
1 2 3 4
1 2 3 5
5
 */
public class Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//столбцы и строки
        int[][] a1 = new int[n][n];
        int[][] a2 = new int[n - 1][n - 1];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a1[j][i] = sc.nextInt();
            }
        }
        int p = sc.nextInt();//число Р
        int w = 0;
        int q = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (p == a1[i][j]) {
                    w = i;
                    q = j;
                }
            }
        }
        int k = -1, l = -1;
        for (int i = 0; i < n; i++) {
            if (i == w) {
                continue;
            }
            k++;
            for (int j = 0; j < n; j++) {
                if (j == q) {
                    continue;
                }
                l++;
                a2[k][l] = a1[j][i];
            }
            l = -1;
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                if (j < n - 2) {
                    System.out.print(a2[i][j] + " ");
                } else {
                    System.out.print(a2[i][j]);
                }
            }
            System.out.println();
        }
    }
}
