package Java12_BM_W5_DZ_2_Part_2;

import java.util.Scanner;

/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Затем сам передается двумерный массив, состоящий из
натуральных чисел.
Необходимо сохранить в одномерном массиве и вывести на экран
минимальный элемент каждой строки.
Пример:
Входные данные  Выходные данные
3 2             10 5
10 20 15
7 5 9

1 3             30 42 15
30
42
15
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int[][] a = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = sc.nextInt();
            }            
        }
        int[] min = new int[m];
        for (int i = 0; i < m; i++) {
            min[i] = 1000;
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {//Находим минимум в каждой
                if (min[i] > a[i][j]) {
                    min[i] = a[i][j];
                }
            }
        }
        for (int i = 0; i < m; i++) {
            System.out.print(min[i] + " ");
        }
    }
}
