package Java12_BM_W5_DZ_2_Part_2;

import java.util.Scanner;

/*

Раз в год Петя проводит конкурс красоты для собак. К сожалению, система
хранения участников и оценок неудобная, а победителя определить надо. В
первой таблице в системе хранятся имена хозяев, во второй - клички животных,
в третьей — оценки трех судей за выступление каждой собаки. Таблицы
связаны между собой только по индексу. То есть хозяин i-ой собаки указан в i-ой
строке первой таблицы, а ее оценки — в i-ой строке третьей таблицы. Нужно
помочь Пете определить топ 3 победителей конкурса.
На вход подается число N — количество участников конкурса. Затем в N
строках переданы имена хозяев. После этого в N строках переданы клички
собак. Затем передается матрица с N строк, 3 вещественных числа в каждой —
оценки судей. Победителями являются три участника, набравшие
максимальное среднее арифметическое по оценкам 3 судей. Необходимо
вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.
Гарантируется, что среднее арифметическое для всех участников будет
различным.
Ограничения:
0 < N < 100
Пример:
Входные данные      Выходные данные
4                   Дарья: Добряш, 9.0
Иван                Николай: Кнопка, 7.6
Николай             Иван: Жучка, 6.6
Анна
Дарья
Жучка
Кнопка
Цезарь
Добряш
7 6 7
8 8 7
4 5 6
9 9 9
*/
public class Task7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int participants = sc.nextInt();
        String[][] arr = new String[participants][2];
        double[][] evaluations = new double[participants][3];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < participants; j++) {
                arr[j][i] = sc.next();
            }
        }
        for (int i = 0; i < participants; i++) {
            for (int j = 0; j < 3; j++) {
                evaluations[i][j] = sc.nextInt();
            }
        }
        double[] sum = new double[participants];
        for (int i = 0; i < participants; i++) {
            for (int j = 0; j < 3; j++) {
                sum[i] += evaluations[i][j];
            }
        }
        int d = 0, k = 0, l = 0;
        for (int i = 0; i < participants - 1; i++) {
            if (sum[i] > sum[i + 1]) {
                d = i;
            } else {
                d = participants - 1;
            }
            for (int j = 0; j < participants; j++) {

                if (sum[j] < sum[d] && sum[j] > sum[j + 1]) {
                    k = j;
                }
                if (sum[j] < sum[k] && sum[j] > sum[j + 1]) {
                    l = j;
                }
            }
        }
        System.out.println(arr[d][0] + ": " + arr[d][1] + ", " + (int) ((sum[d] / 3) * 10) / 10.0);
        System.out.println(arr[k][0] + ": " + arr[k][1] + ", " + (int) ((sum[k] / 3) * 10) / 10.0);
        System.out.print(arr[l][0] + ": " + arr[l][1] + ", " + (int) ((sum[l] / 3) * 10) / 10.0);
    }
}
