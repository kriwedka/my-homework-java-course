package Java12_BM_W5_DZ_2_Part_2;

import java.util.Scanner;

/*
На вход подается число N — количество строк и столбцов матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.
Необходимо вывести true, если она является симметричной относительно
побочной диагонали, false иначе.
Побочной диагональю называется диагональ, проходящая из верхнего правого
угла в левый нижний.
Ограничения:
● 0 < N < 100
● 0 < ai < 1000
Пример:
Входные данные      Выходные данные
3                   false
1 2 3
4 5 6
7 8 9

5                   true
57 190 160 71 42
141 79 187 19 71
141 16 7 187 160
100 42 16 79 190
15 100 141 141 57
 */
public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//столбцы и строки
        int[][] arr = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[j][i] = sc.nextInt();
            }
        }
        boolean sem = true;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (arr[i][j] != arr[(n - 1) - j][(n - 1) - i]) {
                    sem = false;
                }
            }
        }
        System.out.println(sem);
    }
}
