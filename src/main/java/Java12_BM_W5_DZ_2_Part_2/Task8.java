package Java12_BM_W5_DZ_2_Part_2;

import java.util.Scanner;

/*
На вход подается число N. Необходимо посчитать и вывести на экран сумму его
цифр. Решить задачу нужно через рекурсию.
Ограничения:
0 < N < 1000000
Пример:
Входные данные      Выходные данные
12374               17
201                 3
 */
public class Task8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int a = 0;
        for (int i = n; i > 0; i = i / 10) {
            a += n % 10;
            n = n / 10;
        }
        System.out.print(a);
    }
}
