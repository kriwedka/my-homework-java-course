package Java12_BM_W5_DZ_2_Part_2;

import java.util.Scanner;

/*
На вход подается число N. Необходимо вывести цифры числа слева направо.
Решить задачу нужно через рекурсию.
Ограничения:
0 < N < 1000000
Пример:
Входные данные      Выходные данные
12374               1 2 3 7 4
201                 2 0 1
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String n = sc.next();

        for (int i = 0; i < n.length(); i++) {
            System.out.print(n.charAt(i) + " ");
        }
    }
}

