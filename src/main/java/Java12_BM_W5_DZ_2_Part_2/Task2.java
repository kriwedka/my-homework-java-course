package Java12_BM_W5_DZ_2_Part_2;

import java.util.Scanner;

/*
На вход подается число N — количество строк и столбцов матрицы. Затем в
последующих двух строках подаются координаты X (номер столбца) и Y (номер
строки) точек, которые задают прямоугольник.
Необходимо отобразить прямоугольник с помощью символа 1 в матрице,
заполненной нулями (см. пример) и вывести всю матрицу на экран.
Ограничения:
● 0 < N < 100
● 0 <= X1, Y1, X2, Y2 < N
● X1 < X2
● Y1 < Y2
Пример:
Входные данные  Выходные данные
7               0 0 0 0 0 0 0
1 2             0 0 0 0 0 0 0
3 4             0 1 1 1 0 0 0
                0 1 0 1 0 0 0
                0 1 1 1 0 0 0
                0 0 0 0 0 0 0
                0 0 0 0 0 0 0

5               0 1 1 1 1
1 0             0 1 1 1 1
4 1             0 0 0 0 0
                0 0 0 0 0
                0 0 0 0 0

 */
public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        //получаем на вход данные
        int n = sc.nextInt();
        int[][] a = new int[n][n];
        int x1 = sc.nextInt();
        int y1 = sc.nextInt();
        int x2 = sc.nextInt();
        int y2 = sc.nextInt();

        //заполняем единицами от x1 до x2 и от y1 до y2
        for (int i = x1; i <= x2; i++) {
            for (int j = y1; j <= y2; j++) {
                a[i][j] = 1;
            }
        }
        for (int i = x1 + 1; i <= x2 - 1; i++) {
            for (int j = y1 + 1; j <= y2 - 1; j++) {
                a[i][j] = 0;
            }
        }
        //выводим двумерный массив
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n; i++) {
                if (i < n - 1) {
                    System.out.print(a[i][j] + " ");
                } else {
                    System.out.print(a[i][j]);
                }
            }
            System.out.println();
        }
    }
}
