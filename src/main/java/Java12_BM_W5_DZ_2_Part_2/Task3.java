package Java12_BM_W5_DZ_2_Part_2;

import java.util.Scanner;

/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передаются координаты X и Y расположения коня на шахматной доске.
Необходимо заполнить матрицу размера NxN нулями, местоположение коня
отметить символом K, а позиции, которые он может бить, символом X.
Ограничения:
● 4 < N < 100
● 0 <= X, Y < N
Пример:
Входные данные      Выходные данные
5                   0 0 0 0 0
0 4                 0 0 0 0 0
                    0 X 0 0 0
                    0 0 X 0 0
                    K 0 0 0 0

7                   0 0 0 0 0 0 0
3 3                 0 0 X 0 X 0 0
                    0 X 0 0 0 X 0
                    0 0 0 K 0 0 0
                    0 X 0 0 0 X 0
                    0 0 X 0 X 0 0
                    0 0 0 0 0 0 0
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(); //столбцы и строки
        String [][] a = new String[n][n];
        int y = sc.nextInt(); //номер столбца для изменения
        int x = sc.nextInt(); //номер строки для изменения

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[j][i] = "0";
            }
        }
        a[x][y] = "K";
        if (x-1>0&&x-1<n&&y-2>0&&y-2<n) {
            a[x - 1][y - 2] = "X";//1
        }if (x+1>0&&x+1<n&&y-2>0&&y-2<n) {
            a[x + 1][y - 2] = "X";//2
        }if (x-2>0&&x-2<n&&y-1>0&&y-1<n) {
            a[x - 2][y - 1] = "X";//3
        }if (x+2>0&&x+2<n&&y-1>0&&y-1<n) {
            a[x + 2][y - 1] = "X";//4
        }if (x-2>0&&x-2<n&&y+1>0&&y+1<n) {
            a[x - 2][y + 1] = "X";//5
        }if (x+2>0&&x+2<n&&y+1>0&&y+1<n) {
            a[x + 2][y + 1] = "X";//6
        }if (x-1>0&&x-1<n&&y+2>0&&y+2<n) {
            a[x - 1][y + 2] = "X";//7
        }if (x+1>0&&x+1<n&&y+2>0&&y+2<n) {
            a[x + 1][y + 2] = "X";//8
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j < n - 1) {
                    System.out.print(a[i][j] + " ");
                } else {
                    System.out.print(a[i][j]);
                }
            }
            System.out.println();
        }
    }
}
