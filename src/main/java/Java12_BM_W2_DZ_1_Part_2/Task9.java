package Java12_BM_W2_DZ_1_Part_2;

import java.util.Scanner;

/*
Пока Петя практиковался в работе со строками, к нему подбежала его
дочь и спросила: "А правда ли, что тригонометрическое тождество (sin^2(x)+
cos^2(x) - 1 == 0) всегда-всегда выполняется?"
Напишите программу, которая проверяет, что при любом x на входе
тригонометрическое тождество будет выполняться (то есть будет выводить true
при любом x).

Входные данные Выходные данные
90 true
0 true
-200 true
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int equation = (int)(Math.pow(Math.sin(x), 2) + Math.pow(Math.cos(x), 2) - 1);
        if (equation == 0) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }

    }
}
