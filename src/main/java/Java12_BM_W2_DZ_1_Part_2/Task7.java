package Java12_BM_W2_DZ_1_Part_2;

import java.util.Scanner;

//Петя недавно изучил строки в джаве и решил попрактиковаться с ними.
//Ему хочется уметь разделять строку по первому пробелу. Для этого он может
//воспользоваться методами indexOf() и substring().
//На вход подается строка. Нужно вывести две строки, полученные из входной
//разделением по первому пробелу.
//Ограничения:
//В строке гарантированно есть хотя бы один пробел
//Первый и последний символ строки гарантированно не пробел
//2 < s.length() < 100
//Пример:
//Входные данные Выходные данные
//Hi great team!
// Hi
//great team!
//Hello world!
// Hello
//world!
public class Task7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String greeting = sc.nextLine();
        String s1 = greeting.substring(0, greeting.indexOf(" "));
        String s2 = greeting.substring(greeting.indexOf(" ") + 1, greeting.length());
        System.out.println(s1);
        System.out.println(s2);

//          String s1 = sc.next();
//          String s2 = sc.nextLine();
//        System.out.println(s1);
//        System.out.println(s2);
    }
}
