package java12_BM_W2_DZ_1_Part_2;

//За каждый год работы Петя получает на ревью оценку. На вход
//подаются оценки Пети за последние три года (три целых положительных числа).
//Если последовательность оценок строго монотонно убывает, то вывести "Петя,
//пора трудиться"
//В остальных случаях вывести "Петя молодец!"
//Пример:
//Входные данные Выходные данные
//10 5 2 Петя, пора трудиться
//4 20 15 Петя молодец!
//5 5 5 Петя молодец!

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        if (a > b && b > c) {
            System.out.println("Petya, it's time to work.");
        } else {
            System.out.println("Petya is well done!");
        }
    }
}
