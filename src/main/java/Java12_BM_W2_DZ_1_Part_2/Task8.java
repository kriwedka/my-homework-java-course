package Java12_BM_W2_DZ_1_Part_2;

import java.util.Scanner;

/*
Раз так легко получается разделять по первому пробелу, Петя решил
немного изменить предыдущую программу и теперь разделять строку по
последнему пробелу.
Входные данные Выходные данные
Hi great team! Hi great
team!
Hello world! Hello
world!

 */
public class Task8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String greeting = sc.nextLine();
        String s1 = greeting.substring(0, greeting.lastIndexOf(" "));
        String s2 = greeting.substring(greeting.lastIndexOf(" ") + 1, greeting.length());
        System.out.println(s1);
        System.out.println(s2);
    }
}
