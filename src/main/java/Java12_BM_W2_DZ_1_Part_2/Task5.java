package Java12_BM_W2_DZ_1_Part_2;
//Дома дочери Пети опять нужна помощь с математикой! В этот раз ей
//нужно проверить, имеет ли предложенное квадратное уравнение решение или
//нет.
//На вход подаются три числа — коэффициенты квадратного уравнения a, b, c.
//Нужно вывести "Решение есть", если оно есть и "Решения нет", если нет.
//Ограничения:
//-100 < a, b, c < 100
//Пример:
//Входные данные Выходные данные
//1 -95 18 Решение есть
//46 44 3 Решение есть
//34 35 39 Решения нет
//31 -89 4 Решение есть
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        double c = sc.nextDouble();
        double discriminant = b * b - 4 * a * c;
        if (discriminant >= 0) {
            System.out.println("There is a solution");
        } else {
            System.out.println("There is no solution");
        }
    }
}
