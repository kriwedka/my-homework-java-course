package Java13_PM_DZ_2.Task_2;

import java.util.Arrays;

/*
С консоли на вход подается две строки s и t. Необходимо вывести true, если
одна строка является валидной анаграммой другой строки и false иначе.
Анаграмма — это слово или фраза, образованная путем перестановки букв
другого слова или фразы, обычно с использованием всех исходных букв ровно
один раз.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(isAnagram("клоака", "околка"));
    }
    public static boolean isAnagram(String s, String t) {
        char[] word1 = s.replaceAll("\\s", "").toCharArray();
        char[] word2 = t.replaceAll("\\s", "").toCharArray();
        Arrays.sort(word1);
        Arrays.sort(word2);
        return Arrays.equals(word1, word2);
    }
}
