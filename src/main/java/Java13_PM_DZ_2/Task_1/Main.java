package Java13_PM_DZ_2.Task_1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/*
Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
набор уникальных элементов этого массива. Решить используя коллекции.
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<String> list = (ArrayList<String>) Arrays.asList("a", "b", "c", "d", "e");
        convert(list);
    }
    public static <T> HashSet<T> convert(ArrayList<T> from) {
        HashSet<T> result = new HashSet<>();
        result.addAll(from);
        return result;
    }
}
