package Java13_PM_DZ_2.Task_4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static Java13_PM_DZ_2.Task_4.MapDocuments.organizeDocuments;

public class Main {
    public static void main(String[] args) {
        ArrayList<Document> documentArrayList = new ArrayList<>();
        documentArrayList.add(new Document(1,"tax", 5));
        documentArrayList.add(new Document(2,"regular report", 5));
        documentArrayList.add(new Document(3,"presentation", 5));
        documentArrayList.add(new Document(4,"employees salary", 5));

        System.out.println(organizeDocuments(documentArrayList));
    }

//    public static Map<Integer, Document> organizeDocuments(ArrayList<Document> organizeDocumentArray) {
//        Map<Integer, Document> mapOrganizeDocument = new HashMap<>();
//
//        for (Document document : organizeDocumentArray) {
//            mapOrganizeDocument.put(document.getId(), document);
//        }
//        return mapOrganizeDocument;
//    }
}
