package Java13_PM_DZ_2.Task_4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MapDocuments{
    private String document;

    public MapDocuments(int id, String name, int pageCount) {
        super();
    }

    public static Map<Integer, Document> organizeDocuments(ArrayList<Document> organizeDocumentArray) {
        Map<Integer, Document> mapOrganizeDocument = new HashMap<>();

        for (Document document : organizeDocumentArray) {
            mapOrganizeDocument.put(document.getId(), document);
        }
        return mapOrganizeDocument;
    }
}
