package Java13_PM_DZ_1.Task_6;

import java.util.regex.Pattern;

public class FormValidator {
    private static Pattern NAME_PATTERN = Pattern.compile("[A-Z][a-z]{1,19}");
    private static Pattern BIRTH_DATE_PATTERN = Pattern.compile("[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}");
    private static String gender;
    private static String height;

    public FormValidator(String name, String birthDate, String gender, String height) {
        NAME_PATTERN = Pattern.compile(name);
        BIRTH_DATE_PATTERN = Pattern.compile(birthDate);
        this.gender = gender;
        this.height = height;
    }

    public static String checkName(String str) throws Exception {
        if (!NAME_PATTERN.matcher(str).matches()) {
            throw new Exception();
        }
        return str;
    }

    public static String checkBirthdate(String str) throws Exception {
        if (!BIRTH_DATE_PATTERN.matcher(str).matches()) {
            throw new Exception();
        }
        return str;
    }

    public static Object checkGender(String str) throws Exception {
        try {
            Gender.valueOf(str);
        } catch (IllegalArgumentException e) {
            throw new Exception();
        }
        return str;
    }
    public static double checkHeight(String str) throws Exception {
        double height = Double.parseDouble(str);
        if (height < 0) {
            throw new Exception();
        } else {
            return height;
        }
    }
}
