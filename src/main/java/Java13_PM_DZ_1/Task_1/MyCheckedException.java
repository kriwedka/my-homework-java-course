package Java13_PM_DZ_1.Task_1;

public class MyCheckedException extends Exception {
    public MyCheckedException() {
        super("Division by zero occurred");
    }
    public MyCheckedException(String message) {
        super(message);
    }
}
