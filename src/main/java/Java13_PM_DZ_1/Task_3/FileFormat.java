package Java13_PM_DZ_1.Task_3;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Collectors;

public class FileFormat {
    private static final String INPUT_FILE_NAME = "/input.txt";
    private static final String OUTPUT_FILE_NAME = "/output.txt";

    public static void main(String[] args) throws URISyntaxException {
        fileOverwriting();
    }
    public static void fileOverwriting() throws URISyntaxException {
        try (InputStreamReader input = new InputStreamReader(
                Objects.requireNonNull(FileFormat.class.getResourceAsStream(INPUT_FILE_NAME)));
             BufferedWriter bufferedWriter = Files.newBufferedWriter(
                     Paths.get(Objects.requireNonNull(FileFormat.class.getResource(OUTPUT_FILE_NAME)).toURI()));
        ) {
            String result = new BufferedReader(input)
                    .lines().parallel().collect(Collectors.joining("\n"));
            String resultRecord = result.chars()
                    .mapToObj(ch -> (char) ch)
                    .map(Character::toUpperCase)
                    .map(String::valueOf)
                    .collect(Collectors.joining());
            bufferedWriter.write(resultRecord);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
