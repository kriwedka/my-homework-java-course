package Java13_PM_DZ_1.Task_4;

public class MyEvenNumber {
    private int n;
    public MyEvenNumber(int n) {
        if (n % 2 == 1) {
            throw new IllegalArgumentException("Odd number are forbidden");
        } else {
            this.n = n;
        }
    }
}
