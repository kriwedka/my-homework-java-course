package Java13_PM_DZ_1.Task_2;

public class MyUncheckedException extends ArithmeticException {
    public MyUncheckedException() {
        super("You can't divide by zero!");
    }
    public MyUncheckedException(String message) {
        super(message);
    }
}
