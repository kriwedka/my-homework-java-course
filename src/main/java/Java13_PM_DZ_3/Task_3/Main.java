package Java13_PM_DZ_3.Task_3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String args[]) {
        Class<APrinter> cls = APrinter.class;
        try {
            Method methods = cls.getDeclaredMethod("print", int.class);
            methods.invoke(new APrinter(), 23.0);
        } catch (NoSuchMethodException | InvocationTargetException |
                 IllegalAccessException | IllegalArgumentException e) {
            System.out.println(e);
        }
    }
}