package Java13_PM_DZ_3.Task_4;
/*
Написать метод, который с помощью рефлексии получит все интерфейсы
класса, включая интерфейсы от классов-родителей и интерфейсов-родителей.
 */

import java.util.ArrayList;
import java.util.List;

interface A {}
interface B {}
interface C {}
interface D extends A,B {}
interface E extends C,D {}
class Cl1 implements A {}
class Cl2 extends Cl1 implements E {}

public class Main {
    public static void main(String[] args) {
        List<Class<?>> result = getAllInterfaces(Cl2.class);
        System.out.println(Object.class);
        System.out.println("!= " + Cl2.class);
        for (Class<?> anInterface : result) {
            System.out.println(anInterface.getName());
        }
    }
    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            for (Class<?> anInterface: cls.getInterfaces()) {
                interfaces.add(anInterface);
                Class<?>[] arrayInterface = anInterface.getInterfaces();
                while (arrayInterface.length > 0) {
                    for (Class<?> elementInterface: arrayInterface){
                        anInterface = elementInterface;
                        interfaces.add(anInterface);
                        arrayInterface = anInterface.getInterfaces();
                    }
                }
            }
            cls = cls.getSuperclass();
        }
        return interfaces;
    }
}
